import java.util.Scanner;
public class PrintTriangle {
    public static void main(String[] args) {
        Scanner star = new Scanner(System.in);
        System.out.println("Enter n: ");

        int n = star.nextInt();

        for (int i = 1; i <= n; i=i+2){
            for (int k = 1; k<=(n-i)/2; k++)
                System.out.print(" ");
            for (int j = 1; j<= i; j++)
                System.out.print("*");
            for (int l = (n-i)/2 + 1; l <= n; l++)
                System.out.print(" ");
            System.out.print("\n");
        }
    }
}