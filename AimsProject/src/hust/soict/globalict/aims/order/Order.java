package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.utils.MyDate;
import java.util.ArrayList;

public class Order {
	public int id;
	public static final int MAX_NUMBERS_ORDERED = 10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();

	private MyDate dateOrdered;
	private static int nbOrders = 0;
	public static final int MAX_LIMITTED_ORDERS = 5;

	public Order(){
		nbOrders = nbOrders + 1;
		if (nbOrders <= MAX_LIMITTED_ORDERS){
			this.dateOrdered = new MyDate();
			this.id = nbOrders;
		}
		else{
			System.out.println("You can not have more than " + MAX_LIMITTED_ORDERS + " orders");
			nbOrders = nbOrders - 1;
		}
	}
	
	public void addMedia(Media media){
		if (itemsOrdered.size() == MAX_NUMBERS_ORDERED){
			System.out.println("Your order is already full.");
			return;
		}
		else{
			media.setId(itemsOrdered.size() + 1);
			itemsOrdered.add(media);
			System.out.println("Order has been updated. Your order now has "+ itemsOrdered.size() + " items");
		}
	}

	public void removeMedia(int id){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getId() == id)
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in the list.");
        else{
            itemsOrdered.remove(search);
			for (int i = search; i < itemsOrdered.size(); i++) {
				itemsOrdered.get(i).setId(i+1);
			}
            System.out.println("Media removed successfully!");
        }
	}

	public void removeMedia(Media media){
		if (itemsOrdered.size() == 0){
			System.out.println("Your order has no items to be removed.");
			return;
		}
		int search = -1;
		for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getTitle().equals(media.getTitle()))
                search = i;
        }
        if (search == -1)
            System.out.println("The media is not in the list.");
        else{
            itemsOrdered.remove(search);
			for (int i = search; i < itemsOrdered.size(); i++) {
				itemsOrdered.get(i).setId(i+1);
			}
            System.out.println("Media removed successfully!");
        }
	}

	public float totalCost() {
		float sum = 0;
		Media FreeMedia = this.getALuckyItem();
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).equals(FreeMedia))
				System.out.println("Your free item: " + itemsOrdered.get(i).getTitle());
			else
				sum += itemsOrdered.get(i).getCost();
		}
		return sum;
	}

	public void orderPrint(){
		if(itemsOrdered.size() == 0){
			System.out.println("There's no items in the order to display!");
			return;
		}
		System.out.println("***********************Order***********************");
		System.out.print("Date: "); dateOrdered.print();
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			System.out.println(itemsOrdered.get(i).getId() + ". Item - " + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory() + ": " + itemsOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}

	public Media getALuckyItem(){
		int lucky = (int) (Math.random() * itemsOrdered.size());
		return itemsOrdered.get(lucky);
	}
}