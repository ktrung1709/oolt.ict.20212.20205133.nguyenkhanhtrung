package hust.soict.globalict.aims.utils;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.*;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        LocalDate today = LocalDate.now();

        this.day = today.getDayOfMonth();
        this.month = today.getMonthValue();
        this.year = today.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    
    public MyDate(String date) {
        setDate(date);
    }

    public void setDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d['st']['nd']['rd']['th'] y", Locale.ENGLISH);
        LocalDate dateTime = LocalDate.parse(date, formatter);
        setDate(dateTime);
    }

    public void setDate(LocalDate dateTime) {
        this.day = dateTime.getDayOfMonth();
        this.month = dateTime.getMonthValue();
        this.year = dateTime.getYear();
    }

    public LocalDate getLocalDate(){
        return LocalDate.of(year, month, day);
    }

    public void accept() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Date: ");
            String input = sc.nextLine();

            try {
                setDate(input);
                break;
            } catch (Exception e) {
                continue;
            }
        }
        sc.close();
    }

    public void print() {
        LocalDate dateTime = LocalDate.of(year, month, day);
        System.out.println(dateTime);
    }

    public void print(String pattern) {
        LocalDate dateTime = LocalDate.of(year, month, day);
        System.out.println(dateTime.format(DateTimeFormatter.ofPattern(pattern)));
    }

}
