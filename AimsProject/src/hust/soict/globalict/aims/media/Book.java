package hust.soict.globalict.aims.media;

import java.util.*;


public class Book extends Media {

    private List<String> authors = new ArrayList<String>();

    private String content;
    private List<String> contentTokens;
    private Map<String,Integer> wordFrequency;

    public Book(){
    }

    public Book(String title){
        super(title);
    }

    public Book(String title, String category){
        super(title, category);
    }

    public Book(String title, String category, float cost){
        super(title, category, cost);
    }

    public Book(String title, String category, List<String> authors){
        super(title, category);
        this.authors = authors;
    }

    public void addAuthor(String authorName){
        int check = 0;
        for (String i : authors)
            if (i.equals(authorName))
                check++;
        if (check == 0){
            authors.add(authorName);
            System.out.println("Author added successfully!");
        }
        else
            System.out.println("The author is already in the list.");
    }

    public void removeAuthor(String authorName){
        int check = -1;
        for (int i = 0; i < authors.size(); i++) {
            if (authors.get(i).equals(authorName))
                check = i;
        }
        if (check == -1)
            System.out.println("The author is not in the list.");
        else{
            authors.remove(check);
            System.out.println("Author removed successfully!");
        }
 
    }

    public void setContent(String content) {
        this.content = content;
        this.processContent();
    }

    public String getBasicInfo() {
        return super.toString();
    }

    public void processContent(){
        wordFrequency = new TreeMap<>();
        String[] arrayTokens = content.split("\\W+");

        for (String token : arrayTokens) {
            wordFrequency.putIfAbsent(token, 0);
            wordFrequency.put(token, (wordFrequency.get(token) + 1));
        }

        contentTokens = new ArrayList<String>(wordFrequency.keySet());
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\n+ Content: " + content;
        result += "\n+ TokenList: " + contentTokens;
        result += "\n+ Frequency: " + wordFrequency;

        return result;
    }
}