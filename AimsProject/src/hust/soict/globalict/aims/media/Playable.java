package hust.soict.globalict.aims.media;

/**
 * Playable
 */
public interface Playable {
    public void play();
    
}