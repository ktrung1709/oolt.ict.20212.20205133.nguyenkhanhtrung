package hust.soict.globalict.aims.media;
public class DigitalVideoDisc extends Disc implements Playable{
	
	public DigitalVideoDisc(){
		super();
	}

	public DigitalVideoDisc(String title){
        super(title);
    }

    public DigitalVideoDisc(String title, String category){
        super(title, category);
    }

	public DigitalVideoDisc(String title, String category, float cost){
        super(title, category, cost);
    }

	public boolean search(String title){
		int check = 0;
		String[] title_split = title.split(" ");
		for (int i = 0; i < title_split.length; i++)
			if(this.getTitle().toLowerCase().contains(title_split[i].toLowerCase())==false)
				check++;
		if(check == 0)
			return true;
		return false;
	}

	@Override
	public void play(){
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
    public int compareTo(Object obj) {
        Media other = (DigitalVideoDisc) obj;
        if (getCost() < other.getCost())
            return -1;
        if (getCost() > other.getCost())
            return 1;
        return 0;
    }
}
