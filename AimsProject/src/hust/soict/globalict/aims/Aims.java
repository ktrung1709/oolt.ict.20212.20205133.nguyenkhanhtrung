package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;
import java.util.*;

public class Aims {
	
	public static void main(String[] args) {
		MemoryDaemon md = new MemoryDaemon();
    	Thread t1 = new Thread(md);
    	t1.start();
		// Order anOrder = new Order();

		// DigitalVideoDisc dvd1 = new DigitalVideoDisc();
		// dvd1.setTitle("The Lion King");
		// dvd1.setCategory("Animation");
		// dvd1.setCost(19.95f);
		// dvd1.setDirector("Roger Allers");
		// dvd1.setLength(87);

		// anOrder.addMedia(dvd1);

		// DigitalVideoDisc dvd2 = new DigitalVideoDisc();
		// dvd2.setTitle("Star Wars");
		// dvd2.setCategory("Science Fiction");
		// dvd2.setCost(24.95f);
		// dvd2.setDirector("George Lucas");
		// dvd2.setLength(124);

		// anOrder.addMedia(dvd2);
		
		// DigitalVideoDisc dvd3 = new DigitalVideoDisc();
		// dvd3.setTitle("Aladdin");
		// dvd3.setCategory("Animation");
		// dvd3.setCost(18.99f);
		// dvd3.setDirector("John Musker");
		// dvd3.setLength(90);

		// anOrder.addMedia(dvd3);

		// System.out.println(anOrder.totalCost());

		// anOrder.orderPrint();
		
		showMenu();
	}
	
	public static void showMenu() {

		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		do{
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
		switch (choice){
			case 1:
				Order newOrder = new Order();
				userOrder.add(newOrder);
				System.out.println("There's a new order with the id: " + newOrder.id);
				break;
			case 2:
				System.out.println("Input the id of the order you want to add items to: ");
				sc = new Scanner(System.in);
				int inputId = sc.nextInt();
				int search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the number of the items you want to add: ");
				sc = new Scanner(System.in);
				int inputNum = sc.nextInt();
				for (int i = 0; i < inputNum; i++) {
					System.out.println("Enter the type of the item you want to add(Book, CompactDisc or DigitalVideoDisc): ");
					sc = new Scanner(System.in);
					String type = sc.nextLine();
					System.out.println("Enter title: ");
					sc = new Scanner(System.in);
					String title = sc.nextLine();
					System.out.println("Enter category: ");
					sc = new Scanner(System.in);
					String category = sc.nextLine();
					System.out.println("Enter cost: ");
					sc = new Scanner(System.in);
					float cost = sc.nextFloat();

					if (type.equals("Book")){
						Book newMedia = new Book(title, category, cost);
						userOrder.get(search).addMedia(newMedia);
					}
					else if (type.equals("CompactDisc")){
						CompactDisc newMedia = new CompactDisc(title, category, cost);
						System.out.println("Enter the number of tracks: ");
						sc = new Scanner(System.in);
						int numTrack = sc.nextInt();
						for (int j = 0; j < numTrack; j++) {
							System.out.println("Enter title: ");
							sc = new Scanner(System.in);
							title = sc.nextLine();
							System.out.println("Enter length: ");
							sc = new Scanner(System.in);
							int length = sc.nextInt();
							Track newTrack = new Track(title, length);
							newMedia.addTrack(newTrack);
						}
						userOrder.get(search).addMedia(newMedia);
						System.out.println("Do you want to play the CD you just added?");
						sc = new Scanner(System.in);
						String answer = sc.nextLine();
						if(answer.equals("Yes")){
							newMedia.play();
						}
					}
					else if (type.equals("DigitalVideoDisc")){
						DigitalVideoDisc newMedia = new DigitalVideoDisc(title, category, cost);
						userOrder.get(search).addMedia(newMedia);
						System.out.println("Do you want to play the CD you just added?");
						sc = new Scanner(System.in);
						String answer = sc.nextLine();
						if(answer.equals("Yes")){
							newMedia.play();
						}
					}	
				}
				break;
		

			case 3:
				System.out.println("Input the id of the order you want to delete item from: ");
				sc = new Scanner(System.in);
				int orderInputId = sc.nextInt();
				int orderSearch = orderSearch(orderInputId, userOrder);
				if(orderSearch == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the id of the item you want to delete: ");
				sc = new Scanner(System.in);
				int itemId = sc.nextInt();
				userOrder.get(orderSearch).removeMedia(itemId);
				break;

			case 4:
				System.out.println("Input the id of the order you want to display: ");
				sc = new Scanner(System.in);
				inputId = sc.nextInt();
				search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}
				userOrder.get(search).orderPrint();
				break;
		

			case 0:
			System.exit(0);
			return;

		}
		}while (choice != 0);	
	}

	public static int orderSearch(int id, ArrayList<Order> userOrder){
		for (int i = 0; i < userOrder.size(); i++) {
            if (userOrder.get(i).id == id)
                return i;
        }
		return -1;
	}
}