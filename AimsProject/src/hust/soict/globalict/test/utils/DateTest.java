package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.*;
import java.util.*;
public class DateTest {
    public static void main(String[] args) {
        MyDate today = new MyDate();
        today.print();

        MyDate dateInt = new MyDate(18, 9, 2002);
        dateInt.print();

        MyDate dateStr = new MyDate("September 17th 2002");
        dateStr.print();

        MyDate input = new MyDate();
        input.accept();
        input.print();

        MyDate customFormat = new MyDate("September 17th 2002");
        Scanner sc = new Scanner(System.in);
        String pattern = sc.nextLine();
        sc.close();
        customFormat.print(pattern);
        System.out.println(DateUtils.compareDate(today, dateInt));

        MyDate arrDate[] = {today, dateInt, dateStr};
        DateUtils.sortDate(arrDate);
    }
}