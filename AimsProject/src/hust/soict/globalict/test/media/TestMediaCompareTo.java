package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.*;
import java.util.*;

public class TestMediaCompareTo {
    public static void main(String[] args) {
        System.out.println("DVDs:");
        List<DigitalVideoDisc> dvds = new ArrayList<DigitalVideoDisc>();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation",  19.95f);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction",  24.95f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", 18.99f);

        // Add DVD objects to ArrayList
        dvds.add(dvd2);
        dvds.add(dvd1);
        dvds.add(dvd3);

        // Iterate over ArrayList and output titles
        Iterator<DigitalVideoDisc> dvdIterator = dvds.iterator();
        System.out.println("DVDs before sorted:");
        while (dvdIterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) dvdIterator.next()));
        }

        // Sort the collection of DVDs
        Collections.sort((List<DigitalVideoDisc>) dvds);
        System.out.println("-------------------------");
 
        // Iterate over ArrayList and output titles
        dvdIterator = dvds.iterator();
        System.out.println("DVDs after sorted:");
        while (dvdIterator.hasNext()) {
            System.out.println(((DigitalVideoDisc) dvdIterator.next()));
        }
        System.out.println();

        // Add Book objects to ArrayList
        System.out.println("Books:");
        List<Book> books = new ArrayList<Book>();
        books.add(new Book("bk.2", "Bats", 0));
        books.add(new Book("bk.1", "Cats", 0));
        books.add(new Book("bk.3", "Rats", 0));

        // Iterate over ArrayList and output titles
        Iterator<Book> bookIterator = books.iterator();
        System.out.println("Books before sorted:");
        while (bookIterator.hasNext()) {
            System.out.println(((Book) bookIterator.next()).getBasicInfo());
        }

        // Sort the collection of books
        Collections.sort((List<Book>) books);
        System.out.println("-------------------------");

        // Iterate over ArrayList and output titles
        bookIterator = books.iterator();
        System.out.println("Books after sorted:");
        while (bookIterator.hasNext()) {
            System.out.println(((Book) bookIterator.next()).getBasicInfo());
        }
        System.out.println();

        // Add CD objects to ArrayList
        System.out.println("CDs:");
        List<CompactDisc> cds = new ArrayList<CompactDisc>();
        cds.add(new CompactDisc("CD2", "Club", "Club Artist", new ArrayList<>(), 0));
        cds.get(0).addTrack(new Track("T2.1", 3));
        cds.get(0).addTrack(new Track("T2.2", 3));
        cds.add(new CompactDisc("CD1", "Jazz", "Jazz Artist", new ArrayList<>(), 0));
        cds.get(1).addTrack(new Track("T1.1", 3));
        cds.add(new CompactDisc("CD3", "Rock", "Rock Artist", new ArrayList<>(), 0));
        cds.get(2).addTrack(new Track("T3.1", 3));
        cds.get(2).addTrack(new Track("T3.2", 3));
        cds.get(2).addTrack(new Track("T3.3", 3));
        cds.add(new CompactDisc("CD4", "Trap", "Trap Artist", new ArrayList<>(), 0));
        cds.get(3).addTrack(new Track("T4.1", 1));
        cds.get(3).addTrack(new Track("T4.2", 1));
        cds.add(new CompactDisc("CD5", "Folk", "Folk Artist", new ArrayList<>(), 0));
        cds.get(4).addTrack(new Track("T5.1", 1));

        // Iterate over ArrayList and output titles
        Iterator<CompactDisc> cdIterator = cds.iterator();
        System.out.println("Before sorted:");
        while (cdIterator.hasNext()) {
            System.out.println(((CompactDisc) cdIterator.next()));
        }

        // Sort the collection of CDs
        Collections.sort((List<CompactDisc>) cds);
        System.out.println("-------------------------");

        // Iterate over ArrayList and output titles
        cdIterator = cds.iterator();
        System.out.println("After sorted:");
        while (cdIterator.hasNext()) {
            System.out.println(((CompactDisc) cdIterator.next()));
        }
        System.out.println();
    }
}
