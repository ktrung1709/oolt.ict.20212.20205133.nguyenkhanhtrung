package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.Book;

import java.util.*;


public class BookTest {
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        books.add(new Book("bk.2", "Bats", 0));
        books.add(new Book("bk.1", "Cats", 0));
        books.add(new Book("bk.3", "Rats", 0));

        books.get(0).setContent("g v i t q e l u c e c t i i w e v q o g c m o r r a c m e c b m q d v d x v j a i i o w x a l a g p");
        books.get(1).setContent("j u t i l w v z g s p f h j l s k e s z u r r f e p v k j w e v q z q f u e g g s q i j r j b e r z");
        books.get(2).setContent("i i r e v v d h e f k a r m z p d r v o w g p d l v r g t x n s g k x n e q z l n w h k t x d g n a");

        // Iterate over ArrayList and output titles
        System.out.println("Books:");
        for (Book book : books) {
            System.out.println(book);
            System.out.println();
        }
    }
}