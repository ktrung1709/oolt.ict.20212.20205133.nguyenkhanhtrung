package hust.soict.globalict.gui.swing;

import java.awt.*;        // Using AWT layouts
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import javax.swing.*;     // Using Swing components and containers
 
// A Swing GUI application inherits from top-level container javax.swing.JFrame
public class SwingApp extends JFrame {
 
   // Private instance variables
   // ......
 
   // Constructor to setup the GUI components and event handlers
   public SwingApp() {
      // Retrieve the top-level content-pane from JFrame
      Container cp = getContentPane();
 
      // Content-pane sets layout
      cp.setLayout(new ....Layout());
 
      // Allocate the GUI components
      // .....
 
      // Content-pane adds components
      cp.add(....);
 
      // Source object adds listener
      // .....
 
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         // Exit the program when the close-window button clicked
      setTitle("......");  // "super" JFrame sets title
      setSize(300, 150);   // "super" JFrame sets initial size
      setVisible(true);    // "super" JFrame shows
   }
 
   // The entry main() method
   public static void main(String[] args) {
      // Run GUI codes in Event-Dispatching thread for thread-safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            new SwingApp();  // Let the constructor do the job
         }
      });
   }
}
